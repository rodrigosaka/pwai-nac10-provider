package com.fiap.nac.ws;

import java.util.List;

import com.fiap.nac.dao.DAO;
import com.fiap.nac.to.ProdutoTO;

public class EstoqueBO {

	public ProdutoTO consultarProduto(int codProduto){
		
		DAO<ProdutoTO> dao = new DAO<ProdutoTO>(ProdutoTO.class);
		return dao.consultaProduto(codProduto);
		
	}
	
	public void adicionarProduto(ProdutoTO produtoTO){
		
		DAO<ProdutoTO> dao = new DAO<ProdutoTO>(ProdutoTO.class);
		dao.incluiProd(produtoTO);
		
	}
	
	public List<ProdutoTO> listarProdutos(){
		
		DAO<ProdutoTO> dao = new DAO<ProdutoTO>(ProdutoTO.class);
		return dao.listaProd();
		
	}
	
	public void atualizarProduto(ProdutoTO produtoTO){
		
		DAO<ProdutoTO> dao = new DAO<ProdutoTO>(ProdutoTO.class);
		dao.atualizaProd(produtoTO);
		
	}
	
	public void removerProduto(ProdutoTO produtoTO){
		
		DAO<ProdutoTO> dao = new DAO<ProdutoTO>(ProdutoTO.class);
		dao.excluiProd(produtoTO);
		
	}
	
	public List<String> buscarTodasDescricoesProdutos(String descricao){
		
		DAO<ProdutoTO> dao = new DAO<ProdutoTO>(ProdutoTO.class);
		return dao.listaDescProd(descricao);
		
	}
	
	
	
}
