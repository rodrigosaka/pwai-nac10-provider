package com.fiap.nac.to;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name="TB_PRODUTO")
public class ProdutoTO implements Serializable{

	@Id
	@GenericGenerator(name = "seq_produto", strategy = "increment")
	@GeneratedValue(generator = "seq_produto")
	@Column(name="NAC_PRODUTO")
	private int codProduto;
	
	@Column(name="DESC_PRODUTO")
	private String descricao;
	
	@Column(name="QTDE_PRODUTO")
	private int quantidade;
	
	@Column(name="PRECO")
	private double preco;

	public int getCodProduto() {
		return codProduto;
	}

	public void setCodProduto(int codProduto) {
		this.codProduto = codProduto;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public int getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(int quantidade) {
		this.quantidade = quantidade;
	}

	public double getPreco() {
		return preco;
	}

	public void setPreco(double preco) {
		this.preco = preco;
	}
	
	
	
	
	
}
