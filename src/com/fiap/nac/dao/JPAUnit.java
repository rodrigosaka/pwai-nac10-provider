package com.fiap.nac.dao;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class JPAUnit {

	private static EntityManagerFactory emf = Persistence.createEntityManagerFactory("CLIENTE_ORACLE");
	
	public static EntityManager getEntityManager(){
		
		return emf.createEntityManager();
		
	}
	
}
