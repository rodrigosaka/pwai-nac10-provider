package com.fiap.nac.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaQuery;

public class DAO<T> {

	private final Class<T> classe;
	
	public DAO(Class<T> classe){
		this.classe = classe;
	}
	
	public T consultaProduto(int id){
		
		EntityManager em = JPAUnit.getEntityManager();
		return em.find(this.classe, id);
		
	}
	
	public void incluiProd(T objeto){
		
		EntityManager em = JPAUnit.getEntityManager();
		em.getTransaction().begin();
		em.persist(objeto);
		em.getTransaction().commit();
		em.close();
		
	}
	
	public List<T> listaProd(){
		
		EntityManager em = JPAUnit.getEntityManager();
		CriteriaQuery<T> query = em.getCriteriaBuilder().createQuery(classe);
		query.select(query.from(classe));
		List<T> lista = em.createQuery(query).getResultList();
		em.close();
		return lista;
		
	}
	
	public void atualizaProd(T objeto){
		
		EntityManager em = JPAUnit.getEntityManager();
		em.getTransaction().begin();
		em.merge(objeto);
		em.getTransaction().commit();
		em.close();
	}
	
	public void excluiProd(T t){
				
		EntityManager em = JPAUnit.getEntityManager();
		em.getTransaction().begin();
		em.remove(em.merge(t));
		em.getTransaction().commit();
		em.close();
		
	}
	
	public List<String> listaDescProd(String descricao){
		
		EntityManager em = JPAUnit.getEntityManager();
		List<String> lista = em.createQuery("select p.descricao from ProdutoTO p where lower(p.descricao)"
				+ " like :pDesc", String.class).setParameter("pDesc", "%" + descricao.toLowerCase() + "%").getResultList();
		em.close();
		return lista;
		
	}
	
}
